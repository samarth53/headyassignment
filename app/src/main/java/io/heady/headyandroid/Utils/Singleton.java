package io.heady.headyandroid.Utils;

import java.util.List;

import io.heady.headyandroid.Pojos.Product;
import io.heady.headyandroid.Pojos.ProductDetails;
import io.heady.headyandroid.Pojos.Variant;

public class Singleton {

    private static Singleton singleton;
    private Product product;
    private List<ProductDetails> productDetails;
    private List<Variant> variants;

    private Singleton(){

    }

    public static Singleton getInstance(){
        if (singleton == null){
            singleton = new Singleton();
        }
        return singleton;
    }

    public void setProduct(Product product){this.product = product;}

    public Product getProduct(){return product;}

    public void setProductDetails(List<ProductDetails> productDetails){this.productDetails = productDetails;}

    public List<ProductDetails> getProductDetails(){return productDetails;}

    public void setVariants(List<Variant> variants){this.variants = variants;}

    public List<Variant> getVariants(){return variants;}
}
