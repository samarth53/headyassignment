package io.heady.headyandroid.Network;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

public class NetworkImplementation {

    RequestQueue queue;
    private NetworkListenerObject networkListenerObject;

    public NetworkImplementation(Context context, NetworkListenerObject networkListenerObject){
        queue  = Volley.newRequestQueue(context);
        this.networkListenerObject = networkListenerObject;

    }

    public void callJSONObjectAPI(Context context, int requestMethod, String url, JSONObject requestObj, final String alias) {

        Log.d(alias, "URL: "+url);
        if (requestObj != null){
            Log.d(alias, "callJSONObjectAPI: "+requestObj.toString());
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(requestMethod, url, requestObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(alias, "onResponse: "+response);
                networkListenerObject.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(alias, "onErrorResponse: "+error.getMessage() );
                networkListenerObject.onError(error.getMessage());
            }
        });
        queue.add(jsonObjectRequest);
    }

    public interface NetworkListenerObject {
        void onSuccess(JSONObject response);
        void onError(String error);
    }

}
