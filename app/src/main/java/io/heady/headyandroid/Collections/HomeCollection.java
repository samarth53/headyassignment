package io.heady.headyandroid.Collections;

import android.content.Context;

import com.android.volley.Request;
import com.google.gson.Gson;

import org.json.JSONObject;

import io.heady.headyandroid.Helpers.DatabaseHelper;
import io.heady.headyandroid.Network.NetworkImplementation;
import io.heady.headyandroid.Pojos.Category;
import io.heady.headyandroid.Pojos.Product;
import io.heady.headyandroid.Pojos.ProductDetails;
import io.heady.headyandroid.Pojos.Variant;

public class HomeCollection implements NetworkImplementation.NetworkListenerObject {
    private NetworkImplementation networkImplementation;
    private HomeListener homeListener;
    private Context mContext;
    DatabaseHelper databaseHelper;

    public HomeCollection(Context context,HomeListener homeListener){
        mContext = context;
        networkImplementation = new NetworkImplementation(mContext,this);
        this.homeListener = homeListener;
        databaseHelper = new DatabaseHelper(mContext);
    }

    public void getProduct() {
        networkImplementation.callJSONObjectAPI(mContext, Request.Method.GET,"https://stark-spire-93433.herokuapp.com/json",null,"Home");
    }

    private void dbInsert(Product product){
        for (Category cat: product.getCategories()){
            for (ProductDetails pd : cat.getProducts()){
                for (Variant variant : pd.getVariants()){
                    databaseHelper.insertCategory(cat);
                    databaseHelper.insertProduct(pd,cat.getId());
                    databaseHelper.insertVariants(variant,pd.getId());
                }
            }
        }

    }

    @Override
    public void onSuccess(JSONObject response) {

        try{
            Product product = new Gson().fromJson(response.toString(),Product.class);
            dbInsert(product);
            homeListener.onSuccess(product);
            databaseHelper.closeDB();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onError(String error) {
        homeListener.onError(error);
    }

    public interface HomeListener{
        void onSuccess(Product product);
        void onError(String error);
    }
}
