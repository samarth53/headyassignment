package io.heady.headyandroid.Activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.heady.headyandroid.Adapters.RatingsOuterAdapter;
import io.heady.headyandroid.Helpers.DatabaseHelper;
import io.heady.headyandroid.Pojos.ProductDetails;
import io.heady.headyandroid.Pojos.ProductRanking;
import io.heady.headyandroid.Pojos.Ranking;
import io.heady.headyandroid.R;
import io.heady.headyandroid.Utils.Singleton;
import io.heady.headyandroid.databinding.ActivityRatingsBinding;

public class RatingsActivity extends AppCompatActivity {

    private List<Ranking> rankings;
    private List<ProductDetails> productDetails;
    private Context mContext;
    private DatabaseHelper databaseHelper;
    HashMap<String, List<ProductDetails>> rankingsSort;
    ActivityRatingsBinding activityRatingsBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRatingsBinding = DataBindingUtil.setContentView(this,R.layout.activity_ratings);
        mContext = RatingsActivity.this;
        rankingsSort = new HashMap<>();
        databaseHelper = new DatabaseHelper(mContext);
        rankings = Singleton.getInstance().getProduct().getRankings();

        for (Ranking r : rankings){
            productDetails = new ArrayList<>();
            for (ProductRanking pr : r.getProducts()){
                ProductDetails productDetails = new ProductDetails();
                productDetails = databaseHelper.getProducts(pr.getId());
                this.productDetails.add(productDetails);
                Log.d("ranking", r.getRanking());
                Log.d("PD", productDetails.getName());
            }
            rankingsSort.put(r.getRanking(),this.productDetails);
        }

        ArrayList<String> ratingsKeys = new ArrayList<>();
        for (String keys : rankingsSort.keySet()){
            ratingsKeys.add(keys);
        }

        RatingsOuterAdapter ratingsOuterAdapter = new RatingsOuterAdapter(mContext,rankingsSort,ratingsKeys);
        ratingsOuterAdapter.notifyDataSetChanged();
        activityRatingsBinding.rcView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        activityRatingsBinding.rcView.addItemDecoration(new DividerItemDecoration(activityRatingsBinding.rcView.getContext(), DividerItemDecoration.VERTICAL));
        activityRatingsBinding.rcView.setAdapter(ratingsOuterAdapter);
    }
}
