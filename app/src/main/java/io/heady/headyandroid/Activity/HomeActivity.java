package io.heady.headyandroid.Activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;

import org.json.JSONObject;

import io.heady.headyandroid.Adapters.CategoryAdapter;
import io.heady.headyandroid.Collections.HomeCollection;
import io.heady.headyandroid.Pojos.Product;
import io.heady.headyandroid.R;
import io.heady.headyandroid.Utils.Singleton;
import io.heady.headyandroid.databinding.ActivityHomeBinding;

public class HomeActivity extends AppCompatActivity implements HomeCollection.HomeListener {

    ActivityHomeBinding activityHomeBinding;
    private HomeCollection homeCollection;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = HomeActivity.this;
        activityHomeBinding = DataBindingUtil.setContentView(this,R.layout.activity_home);
        activityHomeBinding.progressBar.setVisibility(View.VISIBLE);
        homeCollection = new HomeCollection(mContext,this);
        homeCollection.getProduct();
    }

    @Override
    public void onSuccess(Product product) {
        activityHomeBinding.progressBar.setVisibility(View.GONE);
        Singleton.getInstance().setProduct(product);
        CategoryAdapter categoryAdapter = new CategoryAdapter(mContext, product);
        categoryAdapter.notifyDataSetChanged();
        activityHomeBinding.view.setLayoutManager(new GridLayoutManager(this, 2));
        activityHomeBinding.view.setAdapter(categoryAdapter);
        activityHomeBinding.txtRatings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, RatingsActivity.class));
            }
        });
    }

    @Override
    public void onError(String error) {

    }
}
