package io.heady.headyandroid.Activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import java.util.List;

import io.heady.headyandroid.Adapters.VariantAdapter;
import io.heady.headyandroid.Pojos.Variant;
import io.heady.headyandroid.R;
import io.heady.headyandroid.Utils.Singleton;
import io.heady.headyandroid.databinding.ActivityVariantsBinding;

public class VariantsActivity extends AppCompatActivity implements VariantAdapter.variantListner {

    ActivityVariantsBinding activityVariantsBinding;
    private Context context;
    private List<Variant> variants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityVariantsBinding = DataBindingUtil.setContentView(this,R.layout.activity_variants);
        context = VariantsActivity.this;
        activityVariantsBinding.heading.setText(getIntent().getStringExtra("ProductName"));
        variants = Singleton.getInstance().getVariants();
        VariantAdapter variantAdapter = new VariantAdapter(context,variants,this);
        variantAdapter.notifyDataSetChanged();
        activityVariantsBinding.rcView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        activityVariantsBinding.rcView.setAdapter(variantAdapter);
    }

    @Override
    public void onClick(Variant variant) {
        activityVariantsBinding.Size.setText(variant.getSize().toString());
        activityVariantsBinding.Price.setText(variant.getPrice().toString());
    }
}
