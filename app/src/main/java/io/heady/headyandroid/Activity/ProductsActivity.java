package io.heady.headyandroid.Activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import java.util.List;

import io.heady.headyandroid.Adapters.ProductAdapter;
import io.heady.headyandroid.Pojos.Product;
import io.heady.headyandroid.Pojos.ProductDetails;
import io.heady.headyandroid.R;
import io.heady.headyandroid.Utils.Singleton;
import io.heady.headyandroid.databinding.ActivityProductsBinding;

public class ProductsActivity extends AppCompatActivity {

    ActivityProductsBinding activityProductsBinding;
    private Context mContext;
    private Product product;
    private List<ProductDetails> productDetails;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = ProductsActivity.this;
        activityProductsBinding = DataBindingUtil.setContentView(this,R.layout.activity_products);
        product = Singleton.getInstance().getProduct();
        productDetails = Singleton.getInstance().getProductDetails();
        id = getIntent().getIntExtra("CategoryId",0);
        activityProductsBinding.heading.setText(getIntent().getStringExtra("CategoryName"));
        ProductAdapter productAdapter = new ProductAdapter(mContext,productDetails);
        productAdapter.notifyDataSetChanged();
        activityProductsBinding.view.setLayoutManager(new GridLayoutManager(this, 2));
        activityProductsBinding.view.setAdapter(productAdapter);
        activityProductsBinding.progressBar.setVisibility(View.GONE);

    }
}
