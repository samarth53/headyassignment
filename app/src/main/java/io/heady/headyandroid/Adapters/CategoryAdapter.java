package io.heady.headyandroid.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.heady.headyandroid.Activity.ProductsActivity;
import io.heady.headyandroid.Pojos.Product;
import io.heady.headyandroid.R;
import io.heady.headyandroid.Utils.Singleton;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private Context context;
    private Product product;

    public CategoryAdapter(Context context, Product product){
        this.context = context;
        this.product = product;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.mCategoryText.setText(product.getCategories().get(position).getName());
        holder.mLytCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductsActivity.class);
                intent.putExtra("CategoryId",product.getCategories().get(position).getId());
                intent.putExtra("CategoryName",product.getCategories().get(position).getName());
                Singleton.getInstance().setProductDetails(product.getCategories().get(position).getProducts());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return product.getCategories().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mCategoryText;
        private LinearLayout mLytCategory;
        public ViewHolder(final View itemView) {
            super(itemView);
            mCategoryText = (TextView)itemView.findViewById(R.id.info_text);
            mLytCategory = (LinearLayout)itemView.findViewById(R.id.lytCategory);
        }
    }
}
