package io.heady.headyandroid.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.heady.headyandroid.Pojos.ProductDetails;
import io.heady.headyandroid.R;

public class RatingsInnerAdapter extends RecyclerView.Adapter<RatingsInnerAdapter.ViewHolder> {

    private Context context;
    private List<ProductDetails> rankingsProducts;

    public RatingsInnerAdapter(Context context, List<ProductDetails> rankingsProducts){
        this.context = context;
        this.rankingsProducts = rankingsProducts;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ranking_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.mRankingText.setText(rankingsProducts.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return rankingsProducts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mRankingText;
        public ViewHolder(final View itemView) {
            super(itemView);
            mRankingText = (TextView)itemView.findViewById(R.id.info_text);
        }
    }
}
