package io.heady.headyandroid.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.heady.headyandroid.Activity.ProductsActivity;
import io.heady.headyandroid.Pojos.Product;
import io.heady.headyandroid.Pojos.ProductDetails;
import io.heady.headyandroid.R;
import io.heady.headyandroid.Utils.Singleton;

public class RatingsOuterAdapter extends RecyclerView.Adapter<RatingsOuterAdapter.ViewHolder> {

    private Context context;
    private HashMap<String, List<ProductDetails>> rankingsSort;
    private ArrayList<String> keys;

    public RatingsOuterAdapter(Context context, HashMap<String, List<ProductDetails>> rankingsSort, ArrayList<String> keys){
        this.context = context;
        this.rankingsSort = rankingsSort;
        this.keys = keys;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ranking_heading,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.mRankingText.setText(keys.get(position));
        holder.rcView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        holder.rcView.addItemDecoration(new DividerItemDecoration(holder.rcView.getContext(), DividerItemDecoration.HORIZONTAL));
        RatingsInnerAdapter ratingsInnerAdapter = new RatingsInnerAdapter(context,rankingsSort.get(keys.get(position)));
        ratingsInnerAdapter.notifyDataSetChanged();
        holder.rcView.setAdapter(ratingsInnerAdapter);
    }

    @Override
    public int getItemCount() {
        return rankingsSort.keySet().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mRankingText;
        private RecyclerView rcView;
        public ViewHolder(final View itemView) {
            super(itemView);
            mRankingText = (TextView)itemView.findViewById(R.id.info_text);
            rcView = (RecyclerView)itemView.findViewById(R.id.rcView);
        }
    }
}
