package io.heady.headyandroid.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import io.heady.headyandroid.Activity.ProductsActivity;
import io.heady.headyandroid.Activity.VariantsActivity;
import io.heady.headyandroid.Pojos.Product;
import io.heady.headyandroid.Pojos.ProductDetails;
import io.heady.headyandroid.R;
import io.heady.headyandroid.Utils.Singleton;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private Context context;
    private List<ProductDetails> productDetails;

    public ProductAdapter(Context context, List<ProductDetails> productDetails){
        this.context = context;
        this.productDetails = productDetails;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.mCategoryText.setText(productDetails.get(position).getName());
        holder.mLytCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, VariantsActivity.class);
                intent.putExtra("ProductId",productDetails.get(position).getId());
                intent.putExtra("ProductName",productDetails.get(position).getName());
                Singleton.getInstance().setVariants(productDetails.get(position).getVariants());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mCategoryText;
        private LinearLayout mLytCategory;
        public ViewHolder(final View itemView) {
            super(itemView);
            mCategoryText = (TextView)itemView.findViewById(R.id.info_text);
            mLytCategory = (LinearLayout)itemView.findViewById(R.id.lytCategory);
        }
    }
}
