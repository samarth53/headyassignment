package io.heady.headyandroid.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import io.heady.headyandroid.Pojos.ProductDetails;
import io.heady.headyandroid.Pojos.Variant;
import io.heady.headyandroid.R;

public class VariantAdapter extends RecyclerView.Adapter<VariantAdapter.ViewHolder> {

    private Context context;
    private List<Variant> variants;
    private variantListner variantListner;

    public VariantAdapter(Context context, List<Variant> variants, variantListner variantListner){
        this.context = context;
        this.variants = variants;
        this.variantListner = variantListner;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.variant_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.mCategoryText.setText(variants.get(position).getColor());
        holder.mLytCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                variantListner.onClick(variants.get(position));
                /*Intent intent = new Intent(context, ProductsActivity.class);
                intent.putExtra("CategoryId",product.getCategories().get(position).getId());
                intent.putExtra("CategoryName",product.getCategories().get(position).getName());
                Singleton.getInstance().setProductDetails(product.getCategories().get(position).getProducts());
                context.startActivity(intent);*/

            }
        });

    }

    @Override
    public int getItemCount() {
        return variants.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mCategoryText;
        private LinearLayout mLytCategory;
        public ViewHolder(final View itemView) {
            super(itemView);
            mCategoryText = (TextView)itemView.findViewById(R.id.info_text);
            mLytCategory = (LinearLayout)itemView.findViewById(R.id.lytCategory);
        }
    }

    public interface variantListner{
        void onClick(Variant variant);
    }
}
