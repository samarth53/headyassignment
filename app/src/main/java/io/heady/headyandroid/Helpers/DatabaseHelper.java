package io.heady.headyandroid.Helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import io.heady.headyandroid.Pojos.Category;
import io.heady.headyandroid.Pojos.Product;
import io.heady.headyandroid.Pojos.ProductDetails;
import io.heady.headyandroid.Pojos.Tax;
import io.heady.headyandroid.Pojos.Variant;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "productManager";
    private static final String TABLE_CATEGORIES = "categories";
    private static final String TABLE_PRODUCTS = "products";
    private static final String TABLE_VARIANTS = "variants";

    private static final String CAT_ID = "id";
    private static final String CAT_NAME = "name";

    private static final String PROD_ID = "id";
    private static final String PROD_NAME = "name";
    private static final String PROD_CAT_ID = "cat_id";
    private static final String PROD_TAX_NAME = "tax_name";
    private static final String PROD_TAX_VALUE = "tax_value";


    private static final String PROD_VAR_ID = "prod_id";
    private static final String VAR_ID = "id";
    private static final String VAR_COLOR = "color";
    private static final String VAR_SIZE = "size";
    private static final String VAR_PRICE = "price";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CATEGORY_TABLE = "CREATE TABLE " + TABLE_CATEGORIES + "("
                + CAT_ID + " INTEGER ," + CAT_NAME + " TEXT"
                + ")";

        String CREATE_PRODUCT_TABLE = "CREATE TABLE " + TABLE_PRODUCTS + "("
                + PROD_ID + " INTEGER ," + PROD_NAME + " TEXT ,"
                + PROD_CAT_ID + " INTEGER, "+ PROD_TAX_NAME + " VARCHAR ,"
                + PROD_TAX_VALUE +" DOUBLE " + ")";

        String CREATE_VARIANT_TABLE = "CREATE TABLE " + TABLE_VARIANTS + "("
                + VAR_ID + " INTEGER ," + PROD_VAR_ID + " INTEGER ,"
                + VAR_COLOR + " TEXT ," + VAR_SIZE + " INTEGER ,"
                + VAR_PRICE + " INTEGER " + ")";


        db.execSQL(CREATE_CATEGORY_TABLE);
        db.execSQL(CREATE_PRODUCT_TABLE);
        db.execSQL(CREATE_VARIANT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VARIANTS);
        onCreate(db);
    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public void insertCategory(Category category) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CAT_ID, category.getId());
        values.put(CAT_NAME, category.getName());

        // insert row
        long insert_id = db.insert(TABLE_CATEGORIES, null, values);
    }

    public void insertProduct(ProductDetails product,int categoryID) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PROD_ID, product.getId());
        values.put(PROD_NAME, product.getName());
        values.put(PROD_CAT_ID, categoryID);
        values.put(PROD_TAX_NAME, product.getTax().getName());
        values.put(PROD_TAX_VALUE, product.getTax().getValue());

        // insert row
        long insert_id = db.insert(TABLE_PRODUCTS, null, values);
    }

    public void insertVariants(Variant variant, int productID) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PROD_VAR_ID, productID);
        values.put(VAR_ID, variant.getId());
        values.put(VAR_COLOR, variant.getColor());
        values.put(VAR_SIZE, variant.getSize());
        values.put(VAR_PRICE, variant.getPrice());

        // insert row
        long insert_id = db.insert(TABLE_VARIANTS, null, values);
    }

    public ProductDetails getProducts(int productID){
        //List<ProductDetails> productDetails = new ArrayList<ProductDetails>();
        String query = "SELECT * FROM "+ TABLE_PRODUCTS + " WHERE "+ PROD_ID + " = "+ productID;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null)
            cursor.moveToFirst();
        ProductDetails productDetails = new ProductDetails();
        Tax tax = new Tax();
        productDetails.setId(cursor.getInt(cursor.getColumnIndex(PROD_ID)));
        productDetails.setName(cursor.getString(cursor.getColumnIndex(PROD_NAME)));
        tax.setName(cursor.getString(cursor.getColumnIndex(PROD_TAX_NAME)));
        tax.setValue(cursor.getDouble(cursor.getColumnIndex(PROD_TAX_VALUE)));
        productDetails.setTax(tax);

        /*if (cursor.moveToFirst()){
            do {
                ProductDetails pd = new ProductDetails();
                Tax tax = new Tax();
                pd.setId(cursor.getInt(cursor.getColumnIndex(PROD_ID)));
                pd.setName(cursor.getString(cursor.getColumnIndex(PROD_NAME)));
                tax.setName(cursor.getString(cursor.getColumnIndex(PROD_TAX_NAME)));
                tax.setValue(cursor.getDouble(cursor.getColumnIndex(PROD_TAX_VALUE)));
                pd.setTax(tax);
                productDetails.add(pd);
            }while (cursor.moveToNext());
        }*/
        return productDetails;
    }
}
